Qt 实现组播的代码如下

### 服务器端

```
QUdpSocket socket;
int port = 6000;
socket.bind(QHostAddress::Any, port);
QHostAddress mcast_addr("224.0.0.0");
socket.setSocketOption(QAbstractSocket::MulticastLoopbackOption, 0); // 禁止本机接收
socket.joinMulticastGroup(mcast_addr); // 加入组播地址
```

如果当前系统是双网卡，并且绑定的IP地址是其中一个网卡时，有可能收不到发送的数据。改用QHostAddress::Any能解决这个问题。

如果发生如下错误
```
QAbstractSocket: cannot bind to QHostAddress::Any (or an IPv6 address) and join an IPv4 multicast group
QAbstractSocket: bind to QHostAddress::AnyIPv4 instead if you want to do this
```
把QHostAddress::Any改成QHostAddress::AnyIPv4
```
socket.bind(QHostAddress::AnyIPv4, port);
```

### 客户端

```
QByteArray datagram;
int port = 6000; // 与服务器的监听端口相同
QHostAddress mcast_addr("224.0.0.0"); // 组播地址与服务器设置的地址相同
QUdpSocket socket;
socket.writeDatagram(datagram, mcast_addr, port); // 发送数据
```