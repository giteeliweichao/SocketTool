#SocketTool

TCP/UDP Socket 调试工具，基于Qt & C++ 开发。

- 支持 TCP 客户端
- 支持 UDP 单播
- 支持 UDP 组播
- 支持 UDP 广播
- 暂时还不支持服务器端接受的新连接的操作。





## Wikis

更多信息请查看 [Wikis](https://gitee.com/case/SocketTool/wikis/Home) 。



